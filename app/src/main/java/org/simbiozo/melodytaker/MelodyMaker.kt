package org.simbiozo.melodytaker

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import org.simbiozo.melodytaker.synth.SimpleAudioOutput

class MelodyMaker : ViewModel() {
    class Factory : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return MelodyMaker() as T
        }
    }

    private val frameRate = 48000
    private val noteMaxDurationInSeconds = 5
    private val framesPerBuffer = frameRate * noteMaxDurationInSeconds
    private val samplesPerFrame = SimpleAudioOutput.SAMPLES_PER_FRAME

    private var mTunes: Tunes = Tunes()
    private val mOutputBuffer = FloatArray(framesPerBuffer * samplesPerFrame)
    private val mAudioOutput: SimpleAudioOutput = SimpleAudioOutput()

    var state = State.LET_ADD_TUNE
    var prompt = state.msg

    fun tunes(): Tunes {
        return mTunes
    }

    fun setTunes(tunes: Tunes) {
        mTunes = tunes
    }

    fun addTune() {
        mTunes.addTune(Tune(frameRate, samplesPerFrame, noteMaxDurationInSeconds))
    }

    private fun currentTune(): Tune {
        if (mTunes.isEmpty()) addTune()

        return mTunes.currentTune()
    }

    fun addTrack() {
        currentTune().addTrack()
    }

    fun addNote() {
        currentTune().addNote()
    }

    fun insertNote() {
        currentTune().insertNote()
    }

    fun deleteCurrentNote() {
        currentTune().deleteCurrentNote()
    }

    fun playCurrentNote() {
        mOutputBuffer.fill(0.0f)
        val samples = currentTune().playCurrentNote(mOutputBuffer)
        mAudioOutput.startStream(frameRate)
        mAudioOutput.write(mOutputBuffer, 0, samples)
    }

    fun playCurrentTrack() {
        currentTune().rewind()

        mAudioOutput.startStream(frameRate)

        while (true) {
            mOutputBuffer.fill(0.0f)

            val samples = currentTune().playCurrentTrackNextNote(mOutputBuffer)

            if (samples > 0) {
                mAudioOutput.write(mOutputBuffer, 0, samples)
            } else break
        }
    }

    fun previousNote(): Int {
        return currentTune().previousNote()
    }

    fun nextNote(): Int {
        return currentTune().nextNote()
    }

    fun bpm(): Int {
        return currentTune().bpm()
    }

    fun pitch(): Int {
        return currentTune().pitch()
    }

    fun bpmUp(): Int {
        return currentTune().bpmUp()
    }

    fun bpmDown(): Int {
        return currentTune().bpmDown()
    }

    fun pitchUp(): Int {
        return currentTune().pitchUp()
    }

    fun pitchDown(): Int {
        return currentTune().pitchDown()
    }

    fun tatumsOn(): Int {
        return currentTune().tatumsOn()
    }

    fun tatumsOnUp(): Int {
        return currentTune().tatumsOnUp()
    }

    fun tatumsOnDown(): Int {
        return currentTune().tatumsOnDown()
    }

    fun tatumsOff(): Int {
        return currentTune().tatumsOff()
    }

    fun tatumsOffUp(): Int {
        return currentTune().tatumsOffUp()
    }

    fun tatumsOffDown(): Int {
        return currentTune().tatumsOffDown()
    }

    fun velocity(): Int {
        return currentTune().velocity()
    }

    fun velocityUp(): Int {
        return currentTune().velocityUp()
    }

    fun velocityDown(): Int {
        return currentTune().velocityDown()
    }

    fun commitNote() {
        currentTune().commitNote()
    }

    fun previousTrack(): Int {
        return currentTune().previousTrack()
    }

    fun nextTrack(): Int {
        return currentTune().nextTrack()
    }

    fun previousTune(): Int {
        if (mTunes.isEmpty()) addTune()

       return mTunes.previousTune()
    }

    fun nextTune(): Int {
        if (mTunes.isEmpty()) addTune()

       return mTunes.nextTune()
    }

    fun currentTuneIndex(): Int {
        return mTunes.currentTuneIndex()
    }

    fun enter(ctx: Context, newState: State): String {
        state = newState
        return state.msg
    }

    override fun onCleared() {
        mAudioOutput.stop()
    }

    fun currentTrackIndex(): Int {
        return currentTune().currentTrackIndex()
    }

    fun currentNoteIndex(): Int {
        return currentTune().currentNoteIndex()
    }
}