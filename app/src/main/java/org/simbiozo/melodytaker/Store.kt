package org.simbiozo.melodytaker

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import org.simbiozo.melodytaker.synth.SimpleAudioOutput
import java.io.File
import java.io.FileNotFoundException

class Store {
    private val TAG = "Store"

    fun save(ctx: Context, melodyMaker: MelodyMaker) {
        val json: String = Gson().toJson(melodyMaker.tunes())

        val file = File(ctx.getExternalFilesDir(null), "tunes.json")
        file.writeText(json)
        Log.d(TAG, "tunes.json = $json")
    }

    fun restore(ctx: Context, melodyMaker: MelodyMaker) {

        if(melodyMaker.tunes().isEmpty()) {
            try {
            val file = File(ctx.getExternalFilesDir(null), "tunes.json")
            val json: String = file.readText()
            val tunes: Tunes = Gson().fromJson(json, Tunes::class.java)
                melodyMaker.setTunes(tunes)
            }
            catch (e: FileNotFoundException){
                Log.e(TAG,"FileNotFoundException : tunes.json")
            }
        }
    }
}
