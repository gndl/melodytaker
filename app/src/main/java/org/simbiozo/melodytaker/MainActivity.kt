package org.simbiozo.melodytaker

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.support.v7.app.AppCompatActivity
import android.view.View
import java.util.*


class MainActivity : AppCompatActivity() {
    private lateinit var mMelodyMaker: MelodyMaker
    private lateinit var speaker: TextToSpeech

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mMelodyMaker = ViewModelProvider(this, MelodyMaker.Factory()).get(MelodyMaker::class.java)
        Store().restore(this, mMelodyMaker)

        title = mMelodyMaker.prompt

        speaker = TextToSpeech(applicationContext) { status ->
            if (status != TextToSpeech.ERROR) {
                speaker.language = Locale.UK
            }
        }
    }

    override fun onPause() {
        Store().save(this, mMelodyMaker)
        speaker.stop()
        speaker.shutdown()
        super.onPause()
    }


    fun onUp(view: View) {
        mMelodyMaker.prompt = when (mMelodyMaker.state) {
            State.LET_ADD_TUNE -> enter(State.LET_SELECT_TRACK)
            State.LET_SELECT_TUNE -> enter(State.LET_ADD_TUNE)
            State.LET_ADD_TRACK -> enter(State.LET_SELECT_TUNE)
            State.LET_SELECT_TRACK -> enter(State.LET_ADD_TRACK)

            State.IN_SELECT_TUNE -> reportElement(mMelodyMaker.previousTune())
            State.IN_SELECT_TRACK -> reportElement(mMelodyMaker.previousTrack())

            State.LET_ADD_NOTES -> enter(State.LET_PLAY_TRACK)
            State.LET_SELECT_NOTE -> enter(State.LET_ADD_NOTES)
            State.LET_CHANGE_BPM -> enter(State.LET_SELECT_NOTE)
            State.LET_PLAY_TRACK -> enter(State.LET_CHANGE_BPM)

            State.IN_SELECT_NOTE -> reportNote(mMelodyMaker.previousNote())
            State.IN_CHANGE_BPM -> reportBpm(mMelodyMaker.bpmUp())

            State.LET_CHANGE_PITCH -> enter(State.LET_DELETE_NOTE)
            State.LET_CHANGE_TATUMS_ON -> enter(State.LET_CHANGE_PITCH)
            State.LET_CHANGE_TATUMS_OFF -> enter(State.LET_CHANGE_TATUMS_ON)
            State.LET_CHANGE_VELOCITY -> enter(State.LET_CHANGE_TATUMS_OFF)
            State.LET_INSERT_NOTE -> enter(State.LET_CHANGE_VELOCITY)
            State.LET_DELETE_NOTE -> enter(State.LET_INSERT_NOTE)

            State.IN_CHANGE_PITCH_ADD_NOTE, State.IN_CHANGE_PITCH -> reportNoteValue(mMelodyMaker.pitchUp())
            State.IN_CHANGE_TATUMS_ON -> enterNoteChange(mMelodyMaker.tatumsOnUp(), State.IN_CHANGE_TATUMS_ON)
            State.IN_CHANGE_TATUMS_OFF -> enterNoteChange(mMelodyMaker.tatumsOffUp(), State.IN_CHANGE_TATUMS_OFF)
            State.IN_CHANGE_VELOCITY -> reportNoteValue(mMelodyMaker.velocityUp())
        }
        title = mMelodyMaker.prompt
    }

    fun onDown(view: View) {
        mMelodyMaker.prompt = when (mMelodyMaker.state) {
            State.LET_ADD_TUNE -> enter(State.LET_SELECT_TUNE)
            State.LET_SELECT_TUNE -> enter(State.LET_ADD_TRACK)
            State.LET_ADD_TRACK -> enter(State.LET_SELECT_TRACK)
            State.LET_SELECT_TRACK -> enter(State.LET_ADD_TUNE)

            State.IN_SELECT_TUNE -> reportElement(mMelodyMaker.nextTune())
            State.IN_SELECT_TRACK -> reportElement(mMelodyMaker.nextTrack())

            State.LET_ADD_NOTES -> enter(State.LET_SELECT_NOTE)
            State.LET_SELECT_NOTE -> enter(State.LET_CHANGE_BPM)
            State.LET_CHANGE_BPM -> enter(State.LET_PLAY_TRACK)
            State.LET_PLAY_TRACK -> enter(State.LET_ADD_NOTES)

            State.IN_SELECT_NOTE -> reportNote(mMelodyMaker.nextNote())
            State.IN_CHANGE_BPM -> reportBpm(mMelodyMaker.bpmDown())

            State.LET_CHANGE_PITCH -> enter(State.LET_CHANGE_TATUMS_ON)
            State.LET_CHANGE_TATUMS_ON -> enter(State.LET_CHANGE_TATUMS_OFF)
            State.LET_CHANGE_TATUMS_OFF -> enter(State.LET_CHANGE_VELOCITY)
            State.LET_CHANGE_VELOCITY -> enter(State.LET_INSERT_NOTE)
            State.LET_INSERT_NOTE -> enter(State.LET_DELETE_NOTE)
            State.LET_DELETE_NOTE -> enter(State.LET_CHANGE_PITCH)

            State.IN_CHANGE_PITCH_ADD_NOTE, State.IN_CHANGE_PITCH -> reportNoteValue(mMelodyMaker.pitchDown())
            State.IN_CHANGE_TATUMS_ON -> enterNoteChange(mMelodyMaker.tatumsOnDown(), State.IN_CHANGE_TATUMS_ON)
            State.IN_CHANGE_TATUMS_OFF -> enterNoteChange(mMelodyMaker.tatumsOffDown(), State.IN_CHANGE_TATUMS_OFF)
            State.IN_CHANGE_VELOCITY -> reportNoteValue(mMelodyMaker.velocityDown())
        }
        title = mMelodyMaker.prompt
    }

    fun onRight(view: View) {
        mMelodyMaker.prompt = when (mMelodyMaker.state) {
            State.LET_ADD_TUNE -> addTune()
            State.LET_SELECT_TUNE -> selectTune()
            State.LET_ADD_TRACK -> addTrack()
            State.LET_SELECT_TRACK -> selectTrack()

            State.IN_SELECT_TUNE -> enter(State.LET_SELECT_TRACK)
            State.IN_SELECT_TRACK -> enter(State.LET_ADD_NOTES)

            State.LET_ADD_NOTES -> enterNoteChange(State.IN_CHANGE_PITCH_ADD_NOTE, mMelodyMaker.pitch())
            State.LET_SELECT_NOTE -> selectNote()
            State.LET_CHANGE_BPM -> selectBpm()
            State.LET_PLAY_TRACK -> playTrack()

            State.IN_SELECT_NOTE -> enterNoteChangeSelection(State.LET_CHANGE_PITCH)
            State.IN_CHANGE_BPM -> enter(State.LET_PLAY_TRACK)

            State.LET_CHANGE_PITCH -> enterNoteChange(State.IN_CHANGE_PITCH, mMelodyMaker.pitch())
            State.LET_CHANGE_TATUMS_ON -> enterNoteChange(mMelodyMaker.tatumsOn(), State.IN_CHANGE_TATUMS_ON)
            State.LET_CHANGE_TATUMS_OFF -> enterNoteChange(mMelodyMaker.tatumsOff(), State.IN_CHANGE_TATUMS_OFF)
            State.LET_CHANGE_VELOCITY -> enterNoteChange(State.IN_CHANGE_VELOCITY, mMelodyMaker.velocity())
            State.LET_INSERT_NOTE -> insertNote()
            State.LET_DELETE_NOTE -> deleteNote()

            State.IN_CHANGE_PITCH_ADD_NOTE -> addNote(mMelodyMaker.pitch())
            State.IN_CHANGE_PITCH, State.IN_CHANGE_TATUMS_ON, State.IN_CHANGE_TATUMS_OFF, State.IN_CHANGE_VELOCITY -> commitNote()
        }
        title = mMelodyMaker.prompt
    }

    fun onLeft(view: View) {
        mMelodyMaker.prompt = when (mMelodyMaker.state) {
            State.LET_ADD_TUNE, State.LET_SELECT_TUNE, State.LET_ADD_TRACK, State.LET_SELECT_TRACK -> enter(
                State.LET_ADD_TUNE
            )

            State.IN_SELECT_TUNE, State.IN_SELECT_TRACK, State.LET_ADD_NOTES, State.LET_SELECT_NOTE, State.LET_CHANGE_BPM, State.LET_PLAY_TRACK, State.IN_CHANGE_BPM -> enter(
                State.LET_ADD_TRACK
            )

            State.IN_CHANGE_PITCH_ADD_NOTE, State.IN_SELECT_NOTE -> enter(State.LET_PLAY_TRACK)

            State.LET_CHANGE_PITCH, State.LET_CHANGE_TATUMS_ON, State.LET_CHANGE_TATUMS_OFF, State.LET_CHANGE_VELOCITY,
            State.LET_INSERT_NOTE, State.LET_DELETE_NOTE, State.IN_CHANGE_PITCH, State.IN_CHANGE_TATUMS_ON, State.IN_CHANGE_TATUMS_OFF, State.IN_CHANGE_VELOCITY -> selectNote()
        }
        title = mMelodyMaker.prompt
    }

    private fun enter(state: State): String {
        speaker.speak(state.msg, TextToSpeech.QUEUE_FLUSH, null)
        return mMelodyMaker.enter(this, state)
    }

    private fun addTune(): String {
        mMelodyMaker.addTune()
        return enter(State.LET_ADD_NOTES)
    }

    private fun selectTune(): String {
        val prompt = State.IN_SELECT_TUNE.msg + " " + (mMelodyMaker.currentTuneIndex() + 1)
        speaker.speak(prompt, TextToSpeech.QUEUE_FLUSH, null)
        mMelodyMaker.enter(this, State.IN_SELECT_TUNE)
        return prompt
    }

    private fun reportElement(index: Int): String {
        val prompt = formatStateValue(index + 1)
        speaker.speak(prompt, TextToSpeech.QUEUE_FLUSH, null)
        return prompt
    }

    private fun addTrack(): String {
        mMelodyMaker.addTrack()
        return enter(State.LET_ADD_NOTES)
    }

    private fun selectTrack(): String {
        val prompt = State.IN_SELECT_TRACK.msg + " " + (mMelodyMaker.currentTrackIndex() + 1)
        speaker.speak(prompt, TextToSpeech.QUEUE_FLUSH, null)
        mMelodyMaker.enter(this, State.IN_SELECT_TRACK)
        return prompt
    }

    private fun selectBpm(): String {
        mMelodyMaker.enter(this, State.IN_CHANGE_BPM)
        return reportBpm(mMelodyMaker.bpm())
    }

    private fun reportBpm(bpm: Int): String {
        speaker.speak(bpm.toString(), TextToSpeech.QUEUE_FLUSH, null)
        return bpm.toString() + " " + State.IN_CHANGE_BPM.msg
    }

    private fun addNote(value: Int): String {
        mMelodyMaker.addNote()
        return reportNoteValue(value)
    }

    private fun selectNote(): String {
        mMelodyMaker.enter(this, State.IN_SELECT_NOTE)
        val prompt = formatStateValue(mMelodyMaker.currentNoteIndex() + 1)
        speaker.speak(prompt, TextToSpeech.QUEUE_FLUSH, null)
        mMelodyMaker.playCurrentNote()
        return prompt
    }

    private fun reportNote(noteIndex: Int): String {
        val nn = noteIndex + 1
        val prompt = formatStateValue(nn)

        if (nn == 1) speaker.speak(prompt, TextToSpeech.QUEUE_FLUSH, null)

        mMelodyMaker.playCurrentNote()
        return prompt
    }

    private fun enterNoteChangeSelection(state: State): String {
        val prompt = "${State.IN_SELECT_NOTE.msg} ${mMelodyMaker.currentNoteIndex() + 1} : ${state.msg}"
        speaker.speak(prompt, TextToSpeech.QUEUE_FLUSH, null)
        mMelodyMaker.playCurrentNote()
        mMelodyMaker.enter(this, state)
        return prompt
    }

    private fun reportNoteValue(value: Int): String {
        mMelodyMaker.playCurrentNote()
        return formatStateValue(value)
    }

    private fun enterNoteChange(newState: State, prompt: String): String {
        mMelodyMaker.enter(this, newState)
        speaker.speak(prompt, TextToSpeech.QUEUE_FLUSH, null)
        mMelodyMaker.playCurrentNote()
        return prompt
    }

    private fun enterNoteChange(newState: State, value: Int): String {
        return enterNoteChange(newState, newState.msg + " " + value)
    }

    private fun enterNoteChange(value: Int, newState: State): String {
        return enterNoteChange(newState, value.toString() + " " + newState.msg)
    }

    private fun commitNote(): String {
        mMelodyMaker.commitNote()
        return selectNote()
    }

    private fun insertNote(): String {
        mMelodyMaker.insertNote()
        return enter(State.IN_CHANGE_PITCH)
    }

    private fun deleteNote(): String {
        mMelodyMaker.deleteCurrentNote()
        return selectNote()
    }

    private fun playTrack(): String {
        mMelodyMaker.playCurrentTrack()
        return enter(State.LET_ADD_NOTES)
    }

    private fun formatStateValue(value: Int): String {
        return mMelodyMaker.state.msg + " " + value
    }
}
