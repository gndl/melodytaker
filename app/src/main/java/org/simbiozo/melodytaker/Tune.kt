package org.simbiozo.melodytaker

class Tune(val frameRate: Int, val samplesPerFrame: Int, val noteMaxDurationInSeconds: Int) {
    private val tatumsPerBeat = 4

    private var mBPM = 120
    private var mFramesPerTatum = (frameRate * 60) / (mBPM * tatumsPerBeat)
    private var mCurrentNote = defaultNote()
    private var mCurrentTrackIndex = -1
    private val mTracks: MutableList<Track> = mutableListOf()

    fun addTrack() {
        mTracks.add(Track(samplesPerFrame))
        mCurrentTrackIndex++
    }

    private fun getCurrentTrack(): Track {
        if (mTracks.isEmpty()) addTrack()

        return mTracks[mCurrentTrackIndex]
    }

    fun addNote() {
        getCurrentTrack().addNote(mCurrentNote)
    }

    fun insertNote() {
        getCurrentTrack().insertNote(mCurrentNote)
    }

    fun deleteCurrentNote() {
        getCurrentTrack().deleteCurrentNote()
    }

    fun bpm(): Int {
        return mBPM
    }

    fun bpmUp(): Int {
        mBPM += 10
        mFramesPerTatum = (frameRate * 60) / (mBPM * tatumsPerBeat)
        return mBPM
    }

    fun bpmDown(): Int {
        mBPM -= 10
        mBPM.coerceAtLeast(60 / noteMaxDurationInSeconds)
        mFramesPerTatum = (frameRate * 60) / (mBPM * tatumsPerBeat)
        return mBPM
    }

    fun pitch(): Int {
        return mCurrentNote.pitch
    }

    fun pitchUp(): Int {
        mCurrentNote = mCurrentNote.pitchUp()
        return mCurrentNote.pitch
    }

    fun pitchDown(): Int {
        mCurrentNote = mCurrentNote.pitchDown()
        return mCurrentNote.pitch
    }

    fun tatumsOn(): Int {
        return mCurrentNote.tatumsOn
    }

    fun tatumsOnUp(): Int {
        mCurrentNote = mCurrentNote.tatumsOnUp()
        return mCurrentNote.tatumsOn
    }

    fun tatumsOnDown(): Int {
        mCurrentNote = mCurrentNote.tatumsOnDown()
        return mCurrentNote.tatumsOn
    }

    fun tatumsOff(): Int {
        return mCurrentNote.tatumsOff
    }

    fun tatumsOffUp(): Int {
        mCurrentNote = mCurrentNote.tatumsOffUp()
        return mCurrentNote.tatumsOff
    }

    fun tatumsOffDown(): Int {
        mCurrentNote = mCurrentNote.tatumsOffDown()
        return mCurrentNote.tatumsOff
    }

    fun velocity(): Int {
        return mCurrentNote.velocity
    }

    fun velocityUp(): Int {
        mCurrentNote = mCurrentNote.velocityUp()
        return mCurrentNote.velocity
    }

    fun velocityDown(): Int {
        mCurrentNote = mCurrentNote.velocityDown()
        return mCurrentNote.velocity
    }

    fun rewind() {
        for (track in mTracks) {
            track.rewind()
        }
    }

    fun playCurrentNote(outputBuffer: FloatArray): Int =
        getCurrentTrack().playNote(mCurrentNote, mFramesPerTatum, outputBuffer)

    fun playCurrentTrackNextNote(mOutputBuffer: FloatArray): Int {
        return getCurrentTrack().playNextNote(mFramesPerTatum, mOutputBuffer)
    }

    fun previousNote(): Int {
        mCurrentNote = getCurrentTrack().previousNote()
        return getCurrentTrack().currentNoteIndex()
    }

    fun nextNote(): Int {
        mCurrentNote = getCurrentTrack().nextNote()
        return getCurrentTrack().currentNoteIndex()
    }

    fun commitNote() = getCurrentTrack().setCurrentNote(mCurrentNote)

    fun previousTrack(): Int {
        if (mTracks.isEmpty()) addTrack()

        if (mCurrentTrackIndex > 0) {
            mCurrentTrackIndex--
        } else {
            mCurrentTrackIndex = mTracks.size - 1
        }
        return mCurrentTrackIndex
    }

    fun nextTrack(): Int {
        if (mTracks.isEmpty()) addTrack()

        if (mCurrentTrackIndex < mTracks.size - 1) {
            mCurrentTrackIndex++
        } else {
            mCurrentTrackIndex = 0
        }
        return mCurrentTrackIndex
    }

    fun currentTrackIndex(): Int {
        return mCurrentTrackIndex
    }


    fun currentNoteIndex(): Int {
        return getCurrentTrack().currentNoteIndex()
    }
}
