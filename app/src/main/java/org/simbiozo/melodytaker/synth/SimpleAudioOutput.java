/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.simbiozo.melodytaker.synth;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

/**
 * Simple base class for implementing audio output for examples.
 * This can be sub-classed for experimentation or to redirect audio output.
 */
public class SimpleAudioOutput {
    public static final int SAMPLES_PER_FRAME = 2;
    public static final int BYTES_PER_SAMPLE = 2; // short
    public static final int BYTES_PER_FRAME = SAMPLES_PER_FRAME * BYTES_PER_SAMPLE;
    public static final int BUFFER_SIZE_IN_SECONDS = 1;
    private static final String TAG = "AudioOutputTrack";
    private AudioTrack mAudioTrack;
    private int mFrameRate;
    private int mMode;
    private int mMinBufferSize;
    private int mByteBufferSize;
    private byte[] mByteBuffer;
    private int mShortBufferSize;
    private short[] mShortBuffer;

    /**
     *
     */
    public SimpleAudioOutput() {
        super();
    }

    /**
     * Create an audio track then call play().
     *
     * @param frameRate : the framerate
     */
    public void start(int frameRate) {
        if (mFrameRate != frameRate || mMode != AudioTrack.MODE_STATIC) {
            mAudioTrack = createAudioTrack(frameRate, AudioTrack.MODE_STATIC);
        }
    }

    public void startStream(int frameRate) {
        if (mFrameRate != frameRate || mMode != AudioTrack.MODE_STREAM) {
            mAudioTrack = createAudioTrack(frameRate, AudioTrack.MODE_STREAM);
            mAudioTrack.play();
        }
    }

    public AudioTrack createAudioTrack(int frameRate, int mode) {
        stop();
        mFrameRate = frameRate;
        mMode = mode;

        mByteBufferSize = AudioTrack.getMinBufferSize(frameRate,
                AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT);
        mMinBufferSize = mByteBufferSize / BYTES_PER_SAMPLE;
        mShortBufferSize = mByteBufferSize / BYTES_PER_SAMPLE;

        AudioTrack player = new AudioTrack(AudioManager.STREAM_MUSIC,
                mFrameRate, AudioFormat.CHANNEL_OUT_STEREO,
                AudioFormat.ENCODING_PCM_16BIT, mByteBufferSize,
                mode
        );

        Log.i(TAG, "created AudioTrack : mMode = " + mMode + ", mByteBufferSize = " + mByteBufferSize + ", mMinBufferSize = " + mMinBufferSize);
        return player;
    }

    public int write(float[] buffer, int offset, int length) {
        if (mAudioTrack == null) {
            if (length < mShortBufferSize) {
                mAudioTrack = createAudioTrack(mFrameRate, AudioTrack.MODE_STATIC);
            } else {
                mAudioTrack = createAudioTrack(mFrameRate, AudioTrack.MODE_STREAM);
            }
        }
/*
    if (length < mMinBufferSize && mMode == AudioTrack.MODE_STREAM) {
        mAudioTrack = createAudioTrack(mFrameRate, AudioTrack.MODE_STATIC);
    } else if (length > mMinBufferSize && mMode == AudioTrack.MODE_STATIC) {
        mAudioTrack = createAudioTrack(mFrameRate, AudioTrack.MODE_STREAM);
    }
*/
        float convCx = Short.MAX_VALUE;
        int steps = length / mShortBufferSize;
        int reminder = length - (steps * mShortBufferSize);
        int wsn = 0;

        if (mShortBuffer == null) {
            mShortBuffer = new short[mShortBufferSize];
        }

        for (int stp = 0; stp < steps; stp++) {
            for (int ii = offset + (stp * mShortBufferSize), oi = 0; oi < mShortBufferSize; ii++, oi++) {
                mShortBuffer[oi] = (short) (buffer[ii] * convCx);
            }

            wsn += mAudioTrack.write(mShortBuffer, 0, mShortBufferSize);

            if (mMode == AudioTrack.MODE_STATIC) {
                mAudioTrack.play();
            }
        }

        if (reminder > 0) {
            for (int ii = offset + (steps * mShortBufferSize), oi = 0; oi < reminder; ii++, oi++) {
                mShortBuffer[oi] = (short) (buffer[ii] * convCx);
            }

            wsn += mAudioTrack.write(mShortBuffer, 0, reminder);

            if (mMode == AudioTrack.MODE_STATIC) {
                mAudioTrack.play();
            }
        }

        return wsn;
    }

    public void stop() {
        if (mAudioTrack != null) {
            mAudioTrack.stop();
            mAudioTrack = null;
        }
    }
}
