package org.simbiozo.melodytaker


class Track(samplesPerFrame: Int) {
    private var mInstrument = Instrument()

    private val mNotes: MutableList<Note> = mutableListOf()
    private var mCurrentNoteIndex = 0

    private val mSamplesLevels = FloatArray(samplesPerFrame) { _ -> 0.5f }

    fun addNote(note: Note) {
        mNotes.add(note)
        mCurrentNoteIndex = mNotes.size - 1
    }

    fun insertNote(note: Note) {
        mNotes.add(mCurrentNoteIndex, note)
    }

    fun deleteCurrentNote() {
        mNotes.removeAt(mCurrentNoteIndex)
        if(mCurrentNoteIndex > 0 && mCurrentNoteIndex == mNotes.size) mCurrentNoteIndex--
    }

    fun rewind() {
        mCurrentNoteIndex = 0
    }

    fun setCurrentNote(note: Note) {
        mNotes[mCurrentNoteIndex] = note
    }

    fun playNote(note: Note, framesPerTatum: Int, outputBuffer: FloatArray): Int {

        mInstrument.play(note, framesPerTatum, mSamplesLevels, outputBuffer)

        return (note.tatumsOn + note.tatumsOff) * framesPerTatum * mSamplesLevels.size
    }

    fun playNextNote(framesPerTatum: Int, outputBuffer: FloatArray): Int {

        if (mCurrentNoteIndex >= mNotes.size) {
            if(mCurrentNoteIndex > 0) mCurrentNoteIndex--
            return 0
        }

        return playNote(mNotes[mCurrentNoteIndex++], framesPerTatum, outputBuffer)
    }

    fun previousNote(): Note {
        if(mNotes.isEmpty()) addNote(defaultNote())

        if (mCurrentNoteIndex > 0) {
            mCurrentNoteIndex--
        } else {
            mCurrentNoteIndex = mNotes.size - 1
        }
        return mNotes[mCurrentNoteIndex]
    }

    fun nextNote(): Note {
        if(mNotes.isEmpty()) addNote(defaultNote())

        if (mCurrentNoteIndex < mNotes.size - 1) {
            mCurrentNoteIndex++
        } else {
            mCurrentNoteIndex = 0
        }
        return mNotes[mCurrentNoteIndex]
    }

    fun currentNoteIndex(): Int {
        return mCurrentNoteIndex
    }
}