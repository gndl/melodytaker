package org.simbiozo.melodytaker

import org.simbiozo.melodytaker.synth.SineVoice
import org.simbiozo.melodytaker.synth.SynthVoice

class Instrument {
    private val mVoices: MutableList<SineVoice> = mutableListOf(SineVoice())

    fun addVoice(voice: SineVoice) {
        mVoices.add(voice)
    }

    fun play(note: Note, framesPerTatum: Int, samplesLevels:FloatArray, outputBuffer:FloatArray) {

        for (voice in mVoices) {
            voice.noteOn(note.pitch, note.velocity)
            voice.mix(samplesLevels, outputBuffer, note.tatumsOn * framesPerTatum)
        }
    }
}