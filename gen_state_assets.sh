
txt_to_wave () {
#   pico2wave -l en-GB -w $1.wav "$2"

   echo "    $1(\"$2\")," >> app/src/main/java/org/simbiozo/melodytaker/State.kt
}

echo "package org.simbiozo.melodytaker" > app/src/main/java/org/simbiozo/melodytaker/State.kt
echo "enum class State(val msg: String) {" >> app/src/main/java/org/simbiozo/melodytaker/State.kt

txt_to_wave LET_ADD_TUNE "Add tune?"
txt_to_wave LET_SELECT_TUNE "Select tune?"
txt_to_wave LET_ADD_TRACK "Add track?"
txt_to_wave LET_SELECT_TRACK "Select track?"

txt_to_wave IN_SELECT_TUNE "Tune"
txt_to_wave IN_SELECT_TRACK "Track"

txt_to_wave LET_ADD_NOTES "Add notes?"
txt_to_wave LET_SELECT_NOTE "Select note?"
txt_to_wave LET_CHANGE_BPM "Change BPM?"
txt_to_wave LET_PLAY_TRACK "Play track?"

txt_to_wave IN_SELECT_NOTE "Note"
txt_to_wave IN_CHANGE_BPM "BPM"

txt_to_wave LET_CHANGE_PITCH "Change pitch?"
txt_to_wave LET_CHANGE_TATUMS_ON "Change tatums on?"
txt_to_wave LET_CHANGE_TATUMS_OFF "Change tatums off?"
txt_to_wave LET_CHANGE_VELOCITY "Change velocity?"
txt_to_wave LET_INSERT_NOTE "Insert note?"
txt_to_wave LET_DELETE_NOTE "Delete note?"

txt_to_wave IN_CHANGE_PITCH_ADD_NOTE "pitch"
txt_to_wave IN_CHANGE_PITCH "pitch"
txt_to_wave IN_CHANGE_TATUMS_ON "tatums on"
txt_to_wave IN_CHANGE_TATUMS_OFF "tatums off"
txt_to_wave IN_CHANGE_VELOCITY "velocity"

echo "}" >> app/src/main/java/org/simbiozo/melodytaker/State.kt

#for i in *.wav; do ffmpeg -i "$i" -f s16le -acodec pcm_s16le "$(basename "$i" .wav)".raw ; done

#mv *.raw app/src/main/assets/
#rm *.wav
