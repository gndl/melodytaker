package org.simbiozo.melodytaker

data class Note(val pitch: Int, val tatumsOn: Int, val tatumsOff: Int, val velocity: Int) {

    fun pitchUp(): Note {
        return Note(
            pitch + 1,
            tatumsOn,
            tatumsOff,
            velocity
        )
    }

    fun pitchDown(): Note {
        return Note(
            pitch - 1,
            tatumsOn,
            tatumsOff,
            velocity
        )
    }

    fun tatumsOnUp(): Note {
        return Note(
            pitch,
            tatumsOn + 1,
            tatumsOff,
            velocity
        )
    }

    fun tatumsOnDown(): Note {
        return Note(
            pitch,
            (tatumsOn - 1).coerceAtLeast(0),
            tatumsOff,
            velocity
        )
    }

    fun tatumsOffUp(): Note {
        return Note(
            pitch,
            tatumsOn,
            tatumsOff + 1,
            velocity
        )
    }

    fun tatumsOffDown(): Note {
        return Note(
            pitch,
            tatumsOn,
            (tatumsOff - 1).coerceAtLeast(0),
            velocity
        )
    }

    fun velocityUp(): Note {
        return Note(
            pitch,
            tatumsOn,
            tatumsOff,
            velocity + 10
        )
    }

    fun velocityDown(): Note {
        return Note(
            pitch,
            tatumsOn,
            tatumsOff,
            velocity - 10
        )
    }
}

fun defaultNote(): Note {
    return Note(90, 4, 0, 90)
}

