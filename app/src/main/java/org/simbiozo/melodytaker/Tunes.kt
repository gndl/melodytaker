package org.simbiozo.melodytaker

class Tunes {
    private var mCurrentTuneIndex = -1
    private val mTunes: MutableList<Tune> = mutableListOf()

    fun isEmpty(): Boolean {
        return mTunes.isEmpty()
    }

    fun addTune(tune: Tune) {
        mTunes.add(tune)
        mCurrentTuneIndex++
    }

    fun currentTune(): Tune {
       return mTunes[mCurrentTuneIndex]
    }

    fun previousTune(): Int {
        if (mCurrentTuneIndex > 0) {
            mCurrentTuneIndex--
        } else {
            mCurrentTuneIndex = mTunes.size - 1
        }
        return mCurrentTuneIndex
    }

    fun nextTune(): Int {
       if (mCurrentTuneIndex < mTunes.size - 1) {
            mCurrentTuneIndex++
        } else {
            mCurrentTuneIndex = 0
        }
        return mCurrentTuneIndex
    }

    fun currentTuneIndex(): Int {
        return mCurrentTuneIndex
    }
}